﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Event : MonoBehaviour {
	
	public enum Type {wave1, wave2, wave3, wave4, wave5, wave6, wave7, wave8, wave9, zmaga}

    public GameObject windowPrefab;
    Manager manager;
    public bool printWindow;
	PlayerManager player;

    // Use this for initialization
    void Start () {
        manager = GameObject.Find("GameManager").GetComponent<Manager>();
		printWindow = true;
		player = GameObject.Find ("Player").GetComponent<PlayerManager>();
    }

	public void NextDay(){
		player.ProduceFood ();
		player.ProduceGold ();
		player.ConsumeFood ();
		TriggerEvent (player.day);
		player.day++;
	}

    public void TriggerEvent(int eventName) {

        int numberOfMonsters;
        GameObject windowHolder = GameObject.Find("EventWindowHolder").gameObject;

		if (eventName == (int)Type.wave1)
        {
            numberOfMonsters = 1;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.5f));
			InstantiateEventWindow ("Event 1",
									"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
									"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov.");
        }
		else if (eventName == (int)Type.wave2) {
            numberOfMonsters = 5;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.5f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 1f));
			InstantiateEventWindow ("Event 2",
									"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
									"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
        }
		//fgdfgdfgdfgdfgdg
		else if (eventName == (int)Type.wave3) {
			numberOfMonsters = 6;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.5f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 1f,0.8f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.wave4) {
			numberOfMonsters = 7;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.5f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 1.4f,1.2f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.wave5) {
			numberOfMonsters = 9;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters, 0.7f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.wave6) {
			numberOfMonsters = 13;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.5f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 1f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.wave7) {
			numberOfMonsters = 19;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.5f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 0.7f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.wave8) {
			numberOfMonsters = 20;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.8f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 0.5f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.wave9) {
			numberOfMonsters = 30;
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.Blob, numberOfMonsters, 0.9f));
			StartCoroutine(spawnEnemyWave((int)Enemy.Type.EnemyMan, numberOfMonsters-2, 0.6f));
			InstantiateEventWindow ("Event 2",
				"Zgodilo se je nekaj neverjetnega. NAPAD ooooooo",
				"Napadlo nas je " + numberOfMonsters.ToString() + " nasprotnikov, in še " + (numberOfMonsters - 2).ToString() + " nasprotnikov.");
		}
		else if (eventName == (int)Type.zmaga) {
			numberOfMonsters = 5;
			InstantiateEventWindow ("ZMAGA",
				"Zgodilo se je nekaj neverjetnega.",
				"Zmagali smo.");
		}
    }

	private IEnumerator spawnEnemyWave(int enemyNum, int numberOfEnemies, float timeBetweenEnemies, float delay = 0) {
		yield return new WaitForSeconds(delay);
		for (int i = 0; i < numberOfEnemies; i++)
	        {
	            manager.SpawnEnemy(enemyNum);
	            yield return new WaitForSeconds(timeBetweenEnemies);
	        }
    }

	private void InstantiateEventWindow(string eventName, string eventText, string eventDetails){
		GameObject windowHolder = GameObject.Find("EventWindowHolder").gameObject;
		if (printWindow)
		{
			GameObject go = Instantiate(windowPrefab, Camera.main.WorldToScreenPoint(new Vector3(0, 0, 0)), Quaternion.identity);
			go.transform.SetParent(windowHolder.transform);
			go.transform.Find("EventName").GetComponent<Text>().text = eventName;
			go.transform.Find("EventText").GetComponent<Text>().text = eventText;
			go.transform.Find("EventDetails").GetComponent<Text>().text =  eventDetails;
		}
	}

    public void SwitchShowEventWindowButton(){
        printWindow = !printWindow;
    }
}
