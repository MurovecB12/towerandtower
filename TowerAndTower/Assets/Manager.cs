﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public GameObject[] EnemyPrefabs;
    public GameObject[] turretPrefabs;
    public GameObject BasePrefab;
	public GameObject EnemyLairPrefab;
	public GameObject[] RoadElements;
	public GameObject turretGhostPrefab;

	GameObject turretHolder;
	GameObject RoadHolder;
    public Vector3[] potInitialize;
    private Vector3 spawnPlace;
    private Transform enemyParent;
    private Transform turretParent;
    PlayerManager pl;

    public int selectedTurret;
	public GameObject focusTurret;
	public bool placingTurret = false;
	private GameObject turretGhost;


    void Start() {
		potInitialize = new Vector3[] { new Vector3(15, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 5, 0), new Vector3(-2, 5, 0), new Vector3(-8, -3, 0), new Vector3(4.5f,-4,0) };
        spawnPlace = potInitialize[0];
        enemyParent = GameObject.Find("CurrentEnemeis").transform; // da je pod "tabom" currentEnemies
        turretParent = GameObject.Find("CurrentTurrets").transform; // da je pod "tabom" currentEnemies
        Instantiate(BasePrefab, potInitialize[potInitialize.Length - 1],Quaternion.identity); //spawna bazo na koncu poti
		Instantiate(EnemyLairPrefab, potInitialize[0],Quaternion.identity); // Spawna enemy lair na zacetku poti
        pl = GameObject.Find("Player").GetComponent<PlayerManager>();
		turretHolder = GameObject.Find ("CurrentTurrets");
		RoadHolder = GameObject.Find ("Road");
		CreateRoad ();
    }

    void Update()
    {
        // Spawn TURRET !!!!!!!!!! najvrjetneje treba bolš napisat
		if (Input.GetMouseButtonDown (0)) {
			if (placingTurret) {
				Debug.Log ("turret placed");
				CreatTurret(selectedTurret);
				placingTurret = false;
				GameObject.Destroy (turretGhost);
			} else {
				SetFocusTurret ();
			}
		}
		if (placingTurret) {
			Camera c = Camera.main;
			Vector3 mousePos = c.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 1;
			turretGhost.transform.position = mousePos;
		}
    }

	public void ResetGame(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
		
    public void SpawnEnemy(int monster)
    {
        GameObject go = Instantiate(EnemyPrefabs[monster], spawnPlace, Quaternion.identity);
        go.GetComponent<Enemy>().pot = potInitialize;
        go.GetComponent<Enemy>().spawnPlace = spawnPlace;
        go.GetComponent<Enemy>().CreateMonster(monster);
        go.transform.SetParent(enemyParent);
    }

	public bool CheckPlaceTurret(int tur){
		Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 1; // ker je lahko v prefabih kaj drugače, načeloma mora biti z = 1; 
		Bounds turretBox = new Bounds();
		SpriteRenderer turBoxCollider = turretPrefabs [tur].GetComponent<SpriteRenderer> ();
		turretBox.center = mousePos;
		turretBox.size = turBoxCollider.bounds.size;
		//ker je malo turretov, lahko preverimo ali se bodoči turret seka z katerim od ostalih(če jih rata preveč, IZBOLJŠAJ)
		foreach(Transform box in turretHolder.transform){
			if (turretBox.Intersects (box.GetComponent<SpriteRenderer> ().bounds)) {
				return false;
			}
		}
		//Preverimo še da se nismo zaleteli v cesto
		// TO DO podobno kot za turrete, vendar ko sem na hitro naredil ni delalo pravilno 
		/*foreach(Transform box in RoadHolder.transform){
			if (turretBox.Intersects (box.GetComponentInChildren<SpriteRenderer> ().bounds)) {
				Debug.Log ("Zaleteli smo se");
				return false;
			}
		}*/
		return true;
	}

    public void CreatTurret(int tur) {
		if (pl.money >= turretPrefabs[tur].GetComponent<Turret>().cost && pl.avaliablePeople > 0)
        {
			if (CheckPlaceTurret (tur)) {
				Camera c = Camera.main;
				Vector3 pozicijaTurreta = c.ScreenToWorldPoint (Input.mousePosition);
				pozicijaTurreta.z = 1;
				GameObject go = Instantiate (turretPrefabs [tur], pozicijaTurreta, Quaternion.identity);
				go.transform.parent = turretParent;
				pl.money -= go.GetComponent<Turret> ().cost;
			}
        }
    }

    public void ChangeTurretButton(int turretNumber) {
        selectedTurret = turretNumber;
		placingTurret = true;
		Camera c = Camera.main;
		Vector3 mousePos = c.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 1;
		turretGhost = Instantiate (turretGhostPrefab,mousePos,Quaternion.identity);
    }

	//Draw road
	public void CreateRoad(){
		GameObject basicRoad = RoadElements [0];
		Transform roadParent = GameObject.Find ("Road").transform;
		SpriteRenderer basicRoadSpRender = basicRoad.GetComponentInChildren<SpriteRenderer> ();
		float len = basicRoadSpRender.bounds.size.x;
		GameObject tempRoadElement;


		if (potInitialize.Length>0)
		{

			for (int i = 0; i < potInitialize.Length - 1; i++)
			{
				Vector3 start = potInitialize [i];
				Vector3 end = potInitialize [i + 1];
				float vectorLen = (end - start).magnitude;
				tempRoadElement = Instantiate (basicRoad, start, Quaternion.identity, roadParent);
				tempRoadElement.transform.localScale = new Vector3 (vectorLen/len, 1, 1);
				float fi = Vector3.SignedAngle (new Vector3 (-1, 0, 0), end - start,new Vector3(0,0,1));
				tempRoadElement.transform.Rotate (new Vector3 (0, 0, fi));

			}
		}
	}

	//funkcija za highlite area. se pokaže ko kliknemo turret
	public void SetFocusTurret(){
		Debug.Log("sprozil setFocus");
		Ray mousePos = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(mousePos,out hit)) {
			SetAlphaTurretArea (0f);	
			if(hit.collider.transform.parent.gameObject.CompareTag("Turret")){
				focusTurret = hit.collider.transform.parent.gameObject;
			}
			SetAlphaTurretArea (100f);			                                                                                  
		} else {
			if (focusTurret != null) {
				SetAlphaTurretArea (0f);
			}
			focusTurret = null;
		}
	}

	public void SetAlphaTurretArea(float alpha){
		if (focusTurret != null) {
			Color temp = focusTurret.transform.Find("area").GetComponent<SpriteRenderer> ().color;
			temp.a = alpha;
			focusTurret.transform.Find("area").GetComponent<SpriteRenderer> ().color = temp;
		}
	}
}

//zbriši. 605 zapomni si za mamo!