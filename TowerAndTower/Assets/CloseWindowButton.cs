﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseWindowButton : MonoBehaviour {
    public GameObject window;

    public void CloseWindow() {
        GameObject.Destroy(window);
    }

	public void DeactivateWidow(){
		window.SetActive (false);
	}
}
