﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeopleTabManager : MonoBehaviour {

	GameObject peopleTab;
	PlayerManager player;
	GameObject turretHolder;

	private Transform people;
	private Transform miners;
	private Transform farmers;
	private Transform turrets;
	private Transform GoldProduction;
	private Transform FoodProduction;




	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player").GetComponent<PlayerManager> ();
		peopleTab = GameObject.Find ("PeopleTabInfo");
		turretHolder = GameObject.Find ("CurrentTurrets");

		people = peopleTab.transform.Find("NumberOfPeople");
		miners = peopleTab.transform.Find("NumberOfMiners");
		farmers = peopleTab.transform.Find("NumberOfFarmers");
		turrets = peopleTab.transform.Find("NumberOfTurrets");
		GoldProduction = peopleTab.transform.Find("GoldProduction");
		FoodProduction = peopleTab.transform.Find("FoodProduction");

	}
	
	// Update is called once per frame
	void Update () {
		people.transform.Find("Text (1)").GetComponent<Text>().text = player.people.ToString();
		miners.transform.Find("Text (1)").GetComponent<Text>().text = player.goldMiners.ToString();
		farmers.transform.Find("Text (1)").GetComponent<Text>().text = player.farmers.ToString();
		turrets.transform.Find("Text (1)").GetComponent<Text>().text = turretHolder.transform.childCount.ToString();
		GoldProduction.transform.Find("Text (1)").GetComponent<Text>().text = player.getGoldProduction().ToString();
		FoodProduction.transform.Find("Text (1)").GetComponent<Text>().text = player.getFoodProduction().ToString();
	}

	public void IncreasePopButton(){
		if (player.money >= 100) {
			player.people++;
			player.money -= 100;
		}
		else {
			Debug.Log("Premalo denarja. Potrebuješ 100");
		}
	}

	public void IncreaseMinersButton(){
		if (player.money >= 100 && player.avaliablePeople > 0) {
			player.goldMiners++;
			player.money -= 100;
		}
		else {
			Debug.Log("Premalo denarja. Potrebuješ 100");
		}
	}

	public void IncreaseFarmersButton(){
		if (player.money >= 200 && player.avaliablePeople > 0) {
			player.farmers++;
			player.money -= 200;
		}
		else {
			Debug.Log("Premalo denarja. Potrebuješ 200");
		}
	}
}
