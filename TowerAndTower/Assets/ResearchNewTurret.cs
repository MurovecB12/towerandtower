﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchNewTurret : MonoBehaviour {
	public GameObject window;
	Manager manager;
	PlayerManager plManager;

	void Start(){
		manager = GameObject.Find ("GameManager").GetComponent<Manager> ();
		plManager = GameObject.Find ("Player").GetComponent<PlayerManager> ();
	}

	public void OpenWindow(){
		window.SetActive (true);
	}

	public void ResearchTurret(int tur){
		if (tur == (int)Turret.Type.SuperTurret && !window.activeSelf) {
			if (plManager.money >= 800) {
				plManager.money -= 800;
				OpenWindow ();
			}
		}
		if (tur == (int)Turret.Type.miniTurret && !window.activeSelf) {
			if (plManager.money >= 1000) {
				plManager.money -= 1000;
				OpenWindow ();
			}
		}
	}
}
