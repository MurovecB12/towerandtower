﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWindow : MonoBehaviour {

    private bool isMoving;     // Is the camera being panned?
    //private Vector3 mouseOrigin;    // Position of cursor when mouse dragging starts

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.M))
        {
            // Get mouse origin
            //mouseOrigin = Input.mousePosition;
            isMoving = true;
        }

        if (Input.GetKeyUp(KeyCode.M)) isMoving = false;

        if (isMoving) {
            Vector3 pos = Input.mousePosition;
            this.transform.position = pos;
        }
    }
}
