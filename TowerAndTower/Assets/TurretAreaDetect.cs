﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAreaDetect : MonoBehaviour {


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.gameObject.CompareTag("Enemy")) {
            transform.parent.gameObject.GetComponent<Turret>().targets.Add(collision.transform.GetComponent<Enemy>());
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.gameObject.CompareTag("Enemy"))
        {
            transform.parent.gameObject.GetComponent<Turret>().targets.Remove(collision.transform.GetComponent<Enemy>());
        }
    }
}
