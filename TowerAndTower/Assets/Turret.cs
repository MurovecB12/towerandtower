﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Turret : MonoBehaviour
{
	public enum Type { BasicTurret, SuperTurret, miniTurret};

    public int range;
    public int DMG;
    public int atkRate;
    public int cost;
    private GameObject target;
    public List<Enemy> targets;
    private bool hasShot;
    private float relode;
    public PlayerManager player;
    public Manager mng;
    public int TurretDamageStyle = 0;


    // Use this for initialization
    void Start()
    {
        mng = GameObject.Find("GameManager").GetComponent<Manager>();
        int tur = mng.selectedTurret;
        CreateTurret(tur);
    }

    // Update is called once per frame
    void Update()
    {
        if (hasShot)
        {
            StartCoroutine(Shoot()); // ta bo ustavil funkcijo za "relode" cas 5/atkRate trenutno
        }
    }

    public void CreateTurret(int turretType)
    {
        targets = new List<Enemy>();
        hasShot = true;
        ScaleArea(mng.turretPrefabs[turretType].GetComponent<Turret>().range);
    }

    private void ScaleArea(int scale)
    {
        this.transform.GetChild(0).GetComponent<Transform>().localScale = new Vector3(scale, scale, 0);
    }

    private void RotateToTarget(GameObject target)
    {
        Vector3 smer = target.transform.position - this.transform.position;
        float fi = Vector3.Angle(new Vector3(1, 0, 0), smer);
        float vektProd_z = smer.y; // da dolocimo na kateri polovici smo (ker racunamo kot med x-osjo in smerjo Turret->enemy)
        if (vektProd_z > 0)
        {
            this.transform.eulerAngles = new Vector3(0, 0, fi);
        }
        else
        {
            this.transform.eulerAngles = new Vector3(0, 0, -fi);
        }
    }

    private IEnumerator Shoot()
    {
        //Vrjetno to ni najbolša rešitev, je pa edina ki sem se jo spomnil trenutno :D
        if (targets.Count != 0)
        {
            targets.Sort((a, b) => sortType(a,b,TurretDamageStyle)); // lambda funkcija za sortiranje 
            target = targets[0].transform.gameObject;
            RotateToTarget(target);
            Debug.DrawLine(this.transform.position, target.transform.position,Color.black,0.1f); // trenutno da se vidi kaj strelja
            DealDamage(target);
            hasShot = false;
            
            yield return new WaitForSeconds((float)5 / atkRate); //zau nelinearno narasca hitrost. vrjetno treba spremenit.
        }
        hasShot = true;
    }

    private void DealDamage(GameObject target)
    {
        target.GetComponent<Enemy>().health -= DMG;
    }

    // kako želimo da ima turret prioriteto
    private int sortType(Enemy first, Enemy second,int type)
    {
        //target enemy closest to base
        if (type == 0) {
            if (first.priority > second.priority) return -1;
            if (first.priority == second.priority) return 0;
            return 1;
        }
        //target enemy with highest health
        else if (type == 1)
        {
            if (first.health > second.health) return -1;
            if (first.health == second.health) return 0;
            return 1;
        }
        return 1; 
    
    }
}

