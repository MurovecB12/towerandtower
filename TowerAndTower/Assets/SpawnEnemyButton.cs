﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyButton : MonoBehaviour {

    public void Spawn() {
        GameObject go = GameObject.Find("GameManager");
        go.GetComponent<Manager>().SpawnEnemy(0);

    }
}
