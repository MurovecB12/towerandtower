﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {
    public int life;
    public int money;
	public int food;
	public int day;
    public GameObject player;
	GameObject turretHolder;
    private Transform playerLife;
    private Transform playerMoney;
	private Transform playerFood;
	private Transform PlayerDay;
	private Transform playerPeople;
	private Transform playerFoodConsumption;
	private Transform playerAvaliablePeople;

	public int people;
	public int avaliablePeople;
	public int goldMiners;
	public int farmers;

    // Use this for initialization
    void Start () {
        life = 20;
        money = 500;
		food = 25;
		day = 0;
        player = GameObject.Find("PlayerUI");
		turretHolder = GameObject.Find ("CurrentTurrets");
        playerLife = player.transform.Find("Player Life");
        playerMoney = player.transform.Find("Player Money");
		playerFood = player.transform.Find("Player Food");
		PlayerDay = player.transform.Find("Player Day");
		playerPeople = player.transform.Find("People");
		playerFoodConsumption = player.transform.Find("foodConsumption");
		playerAvaliablePeople = player.transform.Find("AvaliablePeople");
		AdjustPlayerUI ();
    }
	
	// Update is called once per frame
	void Update () {
		avaliablePeople = people - goldMiners - farmers - turretHolder.transform.childCount;

		//boljše bi bilo updejtat tekst na spremembi in ne vsak frame!
        playerLife.transform.Find("Text").GetComponent<Text>().text = "Lives: " + life.ToString();
        playerMoney.transform.Find("Text").GetComponent<Text>().text = "Money: " + money.ToString();
		playerFood.transform.Find("Text").GetComponent<Text>().text = "Food: " + food.ToString();
		PlayerDay.transform.Find("Text").GetComponent<Text>().text = "day: " + day.ToString();
		playerPeople.transform.Find("Text").GetComponent<Text>().text = "People: " + people.ToString();
		playerFoodConsumption.transform.Find("Text").GetComponent<Text>().text = "Food Consum: " + getFoodConsumption().ToString();
		playerAvaliablePeople.transform.Find("Text").GetComponent<Text>().text = "Aval ppl: " + avaliablePeople.ToString();

    }

	public int getGoldProduction(){
		return 10 * goldMiners;
	}

	public void ProduceGold(){
		money += getGoldProduction();	
	}

	public int getFoodProduction(){
		return 5 * farmers;
	}

	public void ProduceFood(){
		food += getFoodProduction();
	}

	public int getFoodConsumption(){
		int turretFoodCost = 2*turretHolder.transform.childCount;
		return avaliablePeople + goldMiners + farmers  + turretFoodCost;
	}

	public void ConsumeFood(){
		food = food - getFoodConsumption();
	}

	public void AdjustPlayerUI(){
		int cameraWidth = Camera.main.pixelWidth;
		GameObject PlayerUI = GameObject.Find ("PlayerUI");
		int numberOfElementsInGrid = PlayerUI.transform.childCount;
		PlayerUI.GetComponent<GridLayoutGroup> ().cellSize = new Vector2 ((float)cameraWidth / (float)numberOfElementsInGrid, 25);
	}
}
