﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{ 
    //Tukaj je za dodajat splosne stvari, ki jih imajo vsi "Enemy"
	public enum Type { Blob, EnemyMan};

	public int health;
	public int speed;
    public int monsterWorth;
    public Vector3[] pot;
    public Vector3 spawnPlace;
    public int priority=0;


    public int enemyNumber;
    

	void Start(){
	}

    private void Update()
    {
        this.Move();
        this.CheckHealth();
        this.printHealth();
        priority += speed;
    }

    public void CheckHealth()
    {
        if (health <= 0)
        {
            GameObject.Find("Player").GetComponent<PlayerManager>().money += monsterWorth;
            Destroy(this.transform.gameObject);
        }
    }

    private bool IsInNeighbourhood(Vector3 sredisce)
    {
        if ((this.transform.position - sredisce).magnitude < 0.1)
        {
            return true;
        }
        return false;
    }

    //  !!!!!!!!!!!!! Tukaj se bo dodajalo nove monsterje, plus to da je treba dodat v Manager v enum monsters !!!!!!!!!!!!!!!!!!!!!
    //  !!!!!!!!!!!!! Treba je tudi v Spawn enemy dodat case za usak monster posebi !!!!!!!!!!
    public void CreateMonster(int monsterType) {
        switch (monsterType) {
            case (int)Type.Blob:
                this.speed = 15;
                this.health = 100;
                this.monsterWorth = 20;
                break;
            case (int)Type.EnemyMan:
                this.speed = 10;
                this.health = 220;
                this.monsterWorth = 55;
                break;
            default:
                Debug.Log("Ni podan monster");
                break;
        }
    }
    
    private int pozicijaCilja = 1;
    public Vector3 smer;
    public void Move()
    {
        if (pozicijaCilja < this.pot.Length)
        {
            smer = (this.pot[pozicijaCilja] - this.pot[pozicijaCilja - 1]).normalized;
        }

        if (pozicijaCilja != this.pot.Length)
        {
            if (IsInNeighbourhood(this.pot[pozicijaCilja]))
            {
                this.transform.position = this.pot[pozicijaCilja];
                pozicijaCilja++;
            }
            this.transform.Translate(this.speed * 0.005f * smer); // če je enemy prehitr, uteče iz poti !!!! SPREMENI !!!!
        }

        // !!!!!!!!!!!  Enemy pride do konca poti !!!!!!!!!!
        if (pozicijaCilja == this.pot.Length) {
            EnemyAtEndPath();
        }

    }

    public void printHealth() {
        this.transform.GetChild(0).GetComponent<TextMesh>().text = this.health.ToString();
    }

    public void EnemyAtEndPath() {
        PlayerManager go = GameObject.Find("Player").GetComponent<PlayerManager>();
        go.life -= 1;
        Debug.Log(priority);
        Destroy(this.transform.gameObject);
    }
}
