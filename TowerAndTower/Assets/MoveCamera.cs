﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour
{
    //
    // VARIABLES
    //

    public float turnSpeed = 1.0f;      // Speed of camera turning when mouse moves in along an axis
    public float moveSpeed = 1.0f;       // Speed of the camera when being panned
    public float zoomSpeed = 1.0f;      // Speed of the camera going back and forth
	public float maxZoom = 15f;			// maximum zoom distzance
    private Vector3 mouseOrigin;    // Position of cursor when mouse dragging starts
    private bool isMoving;     // Is the camera being panned?
    private bool isRotating;    // Is the camera being rotated?
    private bool isZooming; // Is the camera zooming

    //
    // UPDATE
    //

    void Update()
    {
        // Get the left mouse button
		if (Input.GetKeyDown(KeyCode.O))
        {
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isRotating = true;
        }

        // Get the right mouse button
        if (Input.GetMouseButtonDown(1))
        {
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isMoving = true;
        }

        //Get middle button
        if (Input.GetMouseButtonDown(2))
        {
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isZooming = true;
        }


        // Disable movements on button release
		if (!Input.GetKey(KeyCode.O)) isRotating = false;
        if (!Input.GetMouseButton(1)) isMoving = false;
        if (!Input.GetMouseButton(2)) isZooming = false;

        // Rotate camera along X and Y axis
        if (isRotating)
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
            if (pos.x > 0)
            {
                transform.Rotate(new Vector3(0, 0, turnSpeed));
            }
            if (pos.x < 0)
            {
                transform.Rotate(new Vector3(0, 0, -turnSpeed));
            }
        }

        // Move the camera on it's XY plane
        if (isMoving)
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            Vector3 move = new Vector3(-pos.x * moveSpeed, -pos.y * moveSpeed, 0);
            transform.Translate(move, Space.Self);
        }

        if (isZooming) {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
            if (pos.y > 0)
            {
				if (Camera.main.orthographicSize + zoomSpeed / 10 <= maxZoom) {
					Camera.main.orthographicSize += zoomSpeed / 10;
				}
            }
            if (pos.y < 0)
            {
				if (Camera.main.orthographicSize + zoomSpeed / 10 > 0) {
					Camera.main.orthographicSize -= zoomSpeed / 10;
				}
            }
        }
    }
}